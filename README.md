# DragonFly BSD Manpage Search

A Firefox WebExtension that allows the user to search manual page into official DragonFly BSD manual page server.
(leaf.dragonflybsd.org)

- https://addons.mozilla.org/addon/dragonfly-bsd-manpage/
